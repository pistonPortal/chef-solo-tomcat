#
# Copyright (c) 2012-2014 Shailendra Singh <shailendra_01@outlook.com>

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

dest_dir = "#{node['piston']['global_download_dir']}\\#{node['piston']['groovy_download_dir']}"

directory dest_dir do
	recursive true
end

dest_filename = "groovy-binary-#{node['piston']['groovy_version']}.zip"
dest_filepath = dest_dir + "\\" + dest_filename

remote_file dest_filepath do
  source "#{node['piston']['groovy_binary_site']}/groovy/maven/" + dest_filename
  # As chef supports only SHA256 checksum, which is not available for many binaries, we will not use checksum 
  # for verifying integrity of remote files.
  # checksum "#{node['piston']['groovy_sha512_checksum']}"
  not_if { ::File.file?(dest_filepath) }
end

piston_zip "#{node['piston']['groovy_install_dir']}" do
  source dest_filepath
  action :unzip
  startLevel 1
  not_if { ::File.directory?("#{node['piston']['groovy_install_dir']}") }
end

env "GROOVY_HOME" do
	value "#{node['piston']['groovy_install_dir']}"
	# not_if "ENV['groovy_HOME'] == node['piston']['groovy_install_dir']"
end

env "PATH" do
	action :modify
	delim ";"
	value "%GROOVY_HOME%\\bin"
	# not_if "ENV['PATH'].include? '%groovy_HOME%\\bin'"
end