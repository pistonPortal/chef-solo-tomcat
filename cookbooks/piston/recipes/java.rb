#
# Copyright (c) 2012-2014 Shailendra Singh <shailendra_01@outlook.com>

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

include_recipe 'windows'

windows_package "#{node['piston']['jdk_install_package_name']}" do
  source "#{node['piston']['jdk_msi_installer']}"
  action :install
end

env "JAVA_HOME" do
	# Chef::Log.info("Current JAVA_HOME : #{ENV['JAVA_HOME']}")
	# Chef::Log.info("New JAVA_HOME : #{node['java']['java_home']}")
	value "#{node['piston']['java_home']}"
	not_if "ENV['JAVA_HOME'] == node['piston']['java_home']"
end