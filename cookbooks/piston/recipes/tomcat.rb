#
# Copyright (c) 2012-2014 Shailendra Singh <shailendra_01@outlook.com>

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# include_recipe 'windows'

dest_dir = "#{node['piston']['global_download_dir']}\\#{node['piston']['tomcat_download_dir']}"

directory dest_dir do
	recursive true
end

dest_filename = "apache-tomcat-#{node['piston']['tomcat_version']}-windows-x64.zip"
dest_filepath = dest_dir + "\\" + dest_filename

remote_file dest_filepath do
  source "#{node['piston']['global_apache_mirror']}//tomcat/tomcat-8/v#{node['piston']['tomcat_version']}/bin/" + dest_filename
  not_if { ::File.file?(dest_filepath) }
end

piston_zip "#{node['piston']['tomcat_install_dir']}" do
  source dest_filepath
  action :unzip
  startLevel 1
  not_if { ::File.directory?("#{node['piston']['tomcat_install_dir']}") }
end

# Setting this variable creates issues if one is running multiple versions of tomcat on a machine.
# So we will not do it.
# env "CATALINA_HOME" do
# 	value "#{node['piston']['tomcat_home']}"
# 	not_if "ENV['CATALINA_HOME'] == node['piston']['tomcat_home']"
# end

batch "Configure tomcat" do
  code <<-EOL
  	ant -f #{run_context.cookbook_collection[cookbook_name].root_dir}\\files\\tomcat\\configTomcat.xml \
  	-DinstallDir=#{node['piston']['tomcat_install_dir']}
  EOL
  not_if { ::File.directory?("#{node['piston']['tomcat_install_dir']}") }
end