1. This is the initial version of a chef-solo-tomcat repo which can be used to install 
Oracle Java, Apache ant, Groovy and Apache Tomcat on a windows box. It has been tested on a windows 7 box ONLY.

2. Following cookbooks should be downloaded/cloned from internet before running this utility
windows

For my testing I had cloned above cookbooks from 'https://github.com/opscode-cookbooks' location.

3. How to use this utility
	a. Download and copy top-level folder chef-solo-tomcat to a location of your choice.
	c. Download/clone required third party cookbooks mentioned in step 2 to chef-solo-tomcat/cookbooks folder.
	c. Go to chef-solo-tomcat directory and issue following command -
		chef-solo -c .chef\solo.rb -j .chef\node.json
		
For installing other versions of softwares installed by this repository, adjust values of attributes defined 
in various json files under roles folder according to your requirement.

Detailed explanation is available here -
http://pistonportal.wordpress.com/2014/07/30/mananging-tomcat-on-windows-using-chef